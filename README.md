# C#2CSV

## Description
This project obtains the Conceptual Graph of C# source code using the [C# GraphConceptsLibrary](https://gitlab.com/aris_project/CSharp_GraphConceptsLibrary) and represents this graph in CSV format.
This data can be read by the [Aris Code Retrieval Project](https://gitlab.com/aris_project/Code_Retrieval).

## Installation
If you only want to use the app as it is, you only need to download the [executable file](https://gitlab.com/aris_project/CSharp2csv/blob/master/CSharp2CSV/bin/Debug/netcoreapp2.0/CSharp2CSV.dll), it is a .dll file so it will work with Windows, and since it is a [.Net Core app 2.0](https://www.microsoft.com/net/download) it can also beused in MacOS/Linux in the terminal with the help of the `dotnet` command.

## Customize
If you would like to clone the repository and tailor it for a project of your own, you will require [visual studio](https://visualstudio.microsoft.com/downloads/) which is available on Windows, MacOS and Linux for C# development.
Double click on the .sln file and you're ready to go!

## Usage
This app runs in the command prompt/terminal, it takes in two arguments:
1) C# source code path.
2) CSV file destination.

You can also add a flag "-a" (can be added anywhere) the app will convert all C# source code files (.cs) in the directory, usefull to generate a corpus. Another flag is "-n", which numerates each CSV output file names instead of using the method name, it can be usefull for generating a large corpus and especially if the name and destination path goes over the character limit for paths (~256).

By default the outputted CSV will be outputted as follows: `<method name>.cs.csv` <br>
*Note: the .cs indicates the method is from a C# source code to differentiate with other programming languages that may be used in the future, as the Java version of this app[link].

**Examples:**
<br>
Single File
```bash
dotnet CSharp2CSV.dll "/Users/corentin/Desktop/AllCode/Program.cs" "/Users/corentin/Desktop/"
```
will return CSV files for each method successfully converted in "Program.cs" with the name `"<method name>.cs.csv"`

Directory
```bash
dotnet CSharp2CSV.dll "/Users/corentin/Desktop/AllCode/" "/Users/corentin/Desktop/" -a
```
Will return CSV files for each method successfully converted in all .cs files in "AllCode" folder with the name `"<method name>.cs.csv"`

Directory numerated output files
```bash
dotnet CSharp2CSV.dll "/Users/corentin/Desktop/AllCode/1.cs" "/Users/corentin/Desktop/" -a -n
```

Will return CSV files for each method successfully converted in all .cs files in "AllCode" folder with the name `"0.cs.csv"`, `"1.cs.csv"`, ..., `"<n-1>.cs.csv"` where n is the total number of methods.

## Runtime
This code was used to convert a corpus of 1,074,965 methods ([available here](https://mega.nz/#!VmAy2aQR!xtnoeMe7LCcfZNZV37oqhvtHTEuknEtcAfaRrhbUd0A)) in 2 hours and 58 minutes on a Windows machine. The runtime may differ due to machine specs and Operating System. You can get the resulting CSVs from the corpus [here](https://mega.nz/#!lzIgBQAQ!soHVncZNsP2WVwzfId7PgCmMEUtyJ8OBXFcnalwTw2E)