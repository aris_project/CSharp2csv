﻿using GraphConceptsLibrary;
using GraphConceptsLibrary.CGraph;
using QuickGraph;
using Roslyn.Compilers;
using Roslyn.Compilers.CSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CSharp2CSV
{
    class Program
    {
        // Global variables, useful for customizing source and destination paths.
        static string CODE_PATH = "";
        static string DESTINATION = "";
        static Boolean NUMERATE = false;

        public static void Main(String[] args)
        {
            // -a flag if all directory needs to be converted
            // else only specified file.
            if (args.Contains("-a"))
            {
                args = args.Where(x => x != "-a").ToArray();

                // -n flag to numerate each outputted csv file name instead of method name.
                // NUMERATE only works with the -a flag.
                if (args.Contains("-n"))
                {
                    NUMERATE = true;
                    args = args.Where(x => x != "-n").ToArray();
                }

                CODE_PATH = args[0];
                DESTINATION = args[1];
                ConvertAll(CODE_PATH);
            }
            else
            {
                args = args.Where(x => x != "-n").ToArray();
                CODE_PATH = args[0];
                DESTINATION = args[1];
                ConvertCode(CODE_PATH);
            }

            Console.WriteLine("All Done!");
        }

        /* Maybe we could directly implement this inside the ConceptualGraph class
         * so inside the GraphConceptsLibrary to somlify the process and even have
         * it to get the class(es) contained in a file and namespaces.
         */

        public static void ConvertCode(String file_path, String out_name = "")
        {
            String code = "";
            using (FileStream fs = File.Open(file_path, FileMode.Open))
            {
                // An attempt to make everything UTF-8 to reduce errors.
                byte[] b = new byte[1024];
                UTF8Encoding temp = new UTF8Encoding(true);

                while (fs.Read(b, 0, b.Length) > 0)
                {
                    code += temp.GetString(b);
                }
            }

            // Temporary fix for some bugs. Fixes are Documented.
            code = new Regex(@"(?<=foreach.*\()(\\w+(<.*>)*)").Replace(code, "int");
            code = new Regex(@"return;").Replace(code, "return null;");
            code = new Regex(@"(?<=return.*)(\+\+)").Replace(code, " += 1");
            // End of Temporary fixes.

            ConceptualGraph cg = ConceptualGraph.loadGraphFromString(code);

            List<ConceptualGraph> methods = get_methods(cg);
            foreach(var method in methods)
            {
                String class_names = "";

                // This commented out code is used to fetch the package and class names 
                // of method from our corpus, some extra might be required in here or in 
                // the GraphConceptsLibrary to get the tracability.

                //try
                //{
                //    class_names = new Regex(@"(?<=\/\/)(.*)").Match(code).ToString();
                //    class_names = class_names.Replace("\n", "");
                //}
                //catch 
                //{
                //    Console.WriteLine("Warning! No class names found.");
                //}

                String method_name = method.Vertices
                                           .Where(x => x.Name.Contains("Method:"))
                                           .ToList()[0].Name.Split(":")[1];

                if (out_name == "")
                {
                    ToCSV(class_names + method_name, method);
                }
                else
                {
                    ToCSV(out_name, method);
                }

            }

        }

        public static void ConvertAll(String directory_path)
        {
            int method_count = 0;
            foreach (var file in Directory.GetFiles(directory_path, "*.cs"))
            {
                Console.WriteLine(file);
                if (NUMERATE)
                {
                    ConvertCode(directory_path + file, method_count.ToString());
                    method_count++;
                }
                else
                {
                    ConvertCode(directory_path + file);
                }
            }
        }

        public static String get_class_name(ConceptualGraph cg)
        {
            try
            {
                return cg.Vertices
                         .Where(x => x.Name.Contains("Class:"))
                         .ToList()[0].Name;
            }
            catch
            {
                return "";
            }
        }

        public static String get_namespace_name(ConceptualGraph cg)
        {
            try
            {
                return cg.Vertices
                     .Where(x => x.Name.Contains("Namespace:"))
                     .ToList()[0].Name;
            }
            catch
            {
                return "";
            }
        }

        public static List<ConceptualGraph> get_methods(ConceptualGraph cg)
        {
            List<ConceptualGraph> graphs = new List<ConceptualGraph>();

            var root = cg.Vertices
                         .Where(x => x.Name.Contains("Block: Root"))
                         .ToList()[0];

            var contains = cg.Edges
                             .Where(
                                 x => 
                                 x.ToString().Contains("Contains"))
                             .ToList()[0].Target;

            var methods = cg.Vertices
                            .Where(x => x.Name.Contains("Method:"))
                            .ToList();
            
            foreach (var method in methods)
            {
                ConceptualGraph sub_graph = new ConceptualGraph();
                sub_graph.AddVerticesAndEdge(new GraphEdge(root, contains));
                sub_graph.AddVerticesAndEdge(new GraphEdge(contains, method));
                get_subgraph(cg, sub_graph, method);
                graphs.Add(sub_graph);
            }
            return graphs;
        }

        public static GraphConcept get_root(ConceptualGraph cg)
        {
            return cg.Vertices.Where(x => x.Name.Contains("Block: Root")).ToList()[0];
        }

        public static void get_subgraph(ConceptualGraph cg, ConceptualGraph sub, GraphConcept node)
        {
            foreach (var edge in cg.Edges.Where(x => x.Source == node).ToList())
            {
                sub.AddVerticesAndEdge(new GraphEdge(node, edge.Target));
                get_subgraph(cg, sub, edge.Target);
            }
        }

        struct Node
        {
            public long NodeID { get; set; }
            public string Type { get; set; }
            public string Name { get; set; }
            public long MethodID { get; set; }
            public string MethodName { get; set; }
        }

        public static GraphConcept FindSource(ConceptualGraph cg, GraphConcept relation)
        {
            foreach (GraphEdge edge in cg.Edges)
            {
                if (edge.Target == relation)
                {
                    return edge.Source;
                }
            }
            return null;
        }

        public static GraphConcept FindTarget(ConceptualGraph cg, GraphConcept relation)
        {
            foreach (GraphEdge edge in cg.Edges)
            {
                if (edge.Source == relation)
                {
                    return edge.Target;
                }
            }
            return null;
        }


        public static void ToCSV(string methodName, ConceptualGraph cg)
        {
            //methodName = methodName.Split(' ')[1].Split('(')[0];

            List<GraphConcept> Relations = new List<GraphConcept>();

            var relation_labels = new[] {
                "Condition",
                "Contains",
                "Parameter",
                "Returns",
                "Depends",
                "Defines"
            };
            int NodeID = 0;

            // Get all the relation nodes in a container.
            foreach (GraphConcept node in cg.Vertices)
            {
                if (relation_labels.Contains(node.Name))
                {
                    Relations.Add(node);
                }
                else
                {
                    node.NodeRank = NodeID++;
                }

            }
            //Console.WriteLine(methodName.Replace(':', '.'));
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(
                DESTINATION +
                methodName.Replace(':', '.').Replace('<', '_').Replace('>', '_') + ".cs.csv", true)
            )
            {
                foreach (GraphConcept relation in Relations)
                {
                    NodeID++;

                    GraphConcept source = FindSource(cg, relation);
                    GraphConcept target = FindTarget(cg, relation);

                    try
                    {
                        file.WriteLine(
                            //$"{GraphID}, " +
                            $"{methodName.Replace(',', ' ').Replace('\n', ' ')}, " +
                            $"{source.Name.Replace(',', ' ').Replace('\n', ' ')}, " +
                            $"{relation.Name.Replace(',', ' ').Replace('\n', ' ')}, " +
                            $"{target.Name.Replace(',', ' ').Replace('\n', ' ')}, " +
                            $"{source.NodeRank}, " +
                            $"{target.NodeRank}"
                        );
                    }
                    catch
                    {
                        Console.WriteLine("WARNING! - " + relation.Name);
                    }
                }
                file.Close();
            }
        }



    }
}
